# Prime Number Generator

## Build
To build the application use the following commands.

`mvn clean install`

## Prime Number Gereration Strategies

Four strategies are available as part of the application. The strategies and their alias for use in REST and CLI apps 
are given below.
 
 Name | alias
 ---| ---
 Simple Divisibility Test Based Algo | simple
 Sieve of Eratosthenes | esieve
 Segmented Sieve of Eratosthenes | segesieve
 Parallel Segmented Sieve of Eratosthenes | parallel

## REST Service

The REST service is available at `http://<ip>:8888/primes`
 
It can be launched by using the following command:

`java -jar target/prime-num-gen-jar-with-dependencies.jar -s`

A GET request to `http://<ip>:8888/primes` return all the prime number queries on the REST server.
 
 A GET request to `http://<ip>:8888/primes?start=1&end=1000` will return a JSON response containing the primes.
 
 Additionally `algo` can be passed for manual selection of prime number generation.
 
 `http://<ip>:8888/primes?start=1&end=1000&algo=esieve`

## CLI App
The prime number generator can be  invoked by using the following commands:

`java -jar target/prime-num-gen-jar-with-dependencies.jar -start 1 -end 1000`

Additionally, `--algo <algo>` can be passed to manually select the prime number generation algorithm.

`java -jar target/prime-num-gen-jar-with-dependencies.jar -start 1 -end 1000 -algo parallel`