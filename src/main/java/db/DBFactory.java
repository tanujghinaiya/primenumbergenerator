package db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBFactory {
    /**
     * Util to Connect to the HSQLDB in-memory database.
     * @return The connection for the database
     */
    public static Connection DBConnection() throws IOException, SQLException, ClassNotFoundException {
        Connection conn;

        // Registering the HSQLDB JDBC driver
        Class.forName("org.hsqldb.jdbc.JDBCDriver");

        // Creating the connection with HSQLDB for prime-number-gen Database
        conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost:9001/prime-number-gen", "sa", "");

        if (conn!= null){
            return conn;
        } else {
            throw new IOException("DB connection failed");
        }
    }
}
