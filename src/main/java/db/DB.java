package db;

import org.hsqldb.Server;

public class DB implements Runnable{
    /**
     * Method to run the in-memory database.
     */
    @Override
    public void run() {
        Server server = new Server();
        server.setDatabaseName(0, "prime-number-gen");
        server.setDatabasePath(0, "mem:prime-number-gen");
        server.setPort(9001);
        server.setSilent(true);
        server.start();
    }
}
