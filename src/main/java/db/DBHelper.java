package db;

import prime.PrimeNumbers;
import prime.PrimeNumbersContract.PrimeNumbersEntry;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;


/**
 * Class for interacting with the database.
 */
public class DBHelper {
    private static final String[] PRIME_NUMBERS_COLUMNS = {
            PrimeNumbersEntry.COLUMN_START,
            PrimeNumbersEntry.COLUMN_END,
            PrimeNumbersEntry.COLUMN_TIME_ELAPSED,
            PrimeNumbersEntry.COLUMN_ALGORITHM,
            PrimeNumbersEntry.COLUMN_PRIMES_COUNT,
            PrimeNumbersEntry.COLUMN_TIMESTAMP
    };

    private static final String CREATE_TABLE_PRIME_NUMBERS_TABLE = "CREATE TABLE IF NOT EXISTS " + PrimeNumbersEntry.TABLE_NAME + " (" +
            PrimeNumbersEntry.COLUMN_ID + " INTEGER IDENTITY PRIMARY KEY," +
            PrimeNumbersEntry.COLUMN_START + " INT NOT NULL," +
            PrimeNumbersEntry.COLUMN_END + " INT NOT NULL," +
            PrimeNumbersEntry.COLUMN_TIME_ELAPSED + " DECIMAL NOT NULL," +
            PrimeNumbersEntry.COLUMN_ALGORITHM + " VARCHAR(50) NOT NULL," +
            PrimeNumbersEntry.COLUMN_PRIMES_COUNT + " INT NOT NULL," +
            PrimeNumbersEntry.COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT now);";

    private static final String DROP_TABLE_PRIME_NUMBERS_TABLE = "DROP TABLE IF EXISTS " + PrimeNumbersEntry.TABLE_NAME + ";";


    /**
     * Creates the table for prime number entries.
     */
    public void createTables() throws SQLException, IOException, ClassNotFoundException {
        Connection conn = DBFactory.DBConnection();
        Statement statement = conn.createStatement();

        int res = statement.executeUpdate(CREATE_TABLE_PRIME_NUMBERS_TABLE);
        conn.commit();
        conn.close();

        System.out.println("Table Created Successfully.");
    }

    /**
     * Drops the prime number table
     */
    public void dropTables() throws SQLException, IOException, ClassNotFoundException {
        Connection conn = DBFactory.DBConnection();
        Statement statement = conn.createStatement();

        int res = statement.executeUpdate(CREATE_TABLE_PRIME_NUMBERS_TABLE);

        if (res > -1){
            conn.commit();
        }

        conn.close();
        System.out.println("Table Dropped Successfully.");
    }

    /**
     * Method to insert an entry into the prime_numbers table
     * @param primeNumbers Object of class PrimeNumbers returned after successfully generating the required prime numbers.
     */
    public void insertPrimeNumbersEntry(PrimeNumbers primeNumbers) throws SQLException, IOException, ClassNotFoundException {
        Connection conn = DBFactory.DBConnection();

        final String query = "INSERT INTO " + PrimeNumbersEntry.TABLE_NAME + " (" +
                PrimeNumbersEntry.COLUMN_START + ", " +
                PrimeNumbersEntry.COLUMN_END + ", " +
                PrimeNumbersEntry.COLUMN_TIME_ELAPSED + ", " +
                PrimeNumbersEntry.COLUMN_ALGORITHM + ", " +
                PrimeNumbersEntry.COLUMN_PRIMES_COUNT + ")" +
                "VALUES (" +
                primeNumbers.getStart() + "," +
                primeNumbers.getEnd() + "," +
                primeNumbers.getElapsedTime() + "," +
                "'" + primeNumbers.getGenerationAlgorithm() + "'," +
                primeNumbers.size() + ");";

        Statement statement = conn.createStatement();
        statement.executeUpdate(query);

        conn.commit();
        conn.close();
    }

    /**
     * Method to retrieve all the entries in the prime numbers table.
     * @return It returns an ArrayList of instantiated PrimeNumber objects not containing the list of prime numbers.
     */
    public ArrayList<PrimeNumbers> getAllPrimeNumberEntries() throws SQLException, IOException, ClassNotFoundException {
        Connection conn = DBFactory.DBConnection();
        Statement statement = conn.createStatement();
        ArrayList<PrimeNumbers> primeNumberEntries = new ArrayList<>();

        final String query = "SELECT * FROM " + PrimeNumbersEntry.TABLE_NAME + ";";
        ResultSet res = statement.executeQuery(query);

        conn.close();

        while (res.next()) {
            int start = res.getInt(PrimeNumbersEntry.COLUMN_START);
            int end = res.getInt(PrimeNumbersEntry.COLUMN_END);
            int n = res.getInt(PrimeNumbersEntry.COLUMN_PRIMES_COUNT);
            float timeElapsed = res.getBigDecimal(PrimeNumbersEntry.COLUMN_TIME_ELAPSED).floatValue();
            String algorithm = res.getString(PrimeNumbersEntry.COLUMN_ALGORITHM);
            String timestamp = res.getTimestamp(PrimeNumbersEntry.COLUMN_TIMESTAMP).toString();

            primeNumberEntries.add(new PrimeNumbers(start, end, n, timeElapsed, algorithm, timestamp));
        }

        return primeNumberEntries;
    }
}
