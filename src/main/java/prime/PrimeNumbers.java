package prime;

import java.util.ArrayList;


/**
 * Model of Prime Numbers
 */
public class PrimeNumbers {
    private int start;

    private int end;

    private ArrayList<Integer> primeNumbers;

    private int count;

    private float elapsedTime;

    private String generationAlgorithm;

    private String timestamp;

    public PrimeNumbers(int start, int end) {
        this.start = start;
        this.end = end;
        this.primeNumbers = new ArrayList<>();
    }

    public PrimeNumbers(int start, int end, int count, float elapsedTime, String generatorAlgorithm, String timestamp) {
        this.start = start;
        this.end = end;
        this.elapsedTime = elapsedTime;
        this.generationAlgorithm = generatorAlgorithm;
        this.count = count;
        this.timestamp = timestamp;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getCount() {
        return count;
//        return primeNumbers == null ? 0 : primeNumbers.size();
    }

    public ArrayList<Integer> getPrimeNumbers() {
        return primeNumbers;
    }

    public void setPrimeNumbers(ArrayList<Integer> primeNumbers) {
        this.primeNumbers = primeNumbers;
    }

    public String getGenerationAlgorithm() {
        return generationAlgorithm;
    }

    public void setGenerationAlgorithm(String generationAlgorithm) {
        this.generationAlgorithm = generationAlgorithm;
    }

    public void setGenerationAlgorithm(Class c) {
        this.generationAlgorithm = c.getSimpleName();
    }

    public int size() {
        if (primeNumbers == null) {
            return 0;
        }

        return primeNumbers.size();
    }

    public float getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(float elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void add(int item) {
        if (primeNumbers != null) {
            primeNumbers.add(item);
        }
    }

    public void addAll(ArrayList<Integer> items) {
        if (primeNumbers != null) {
            primeNumbers.addAll(items);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int prime : primeNumbers) {
            sb.append(prime).append(" ");
        }

        return sb.toString();
    }
}
