package prime;

/**
 * Contract specifying the matadata regarding the database
 */
public class PrimeNumbersContract {
    public static final class PrimeNumbersEntry {
        public static final String TABLE_NAME = "prime_numbers";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_START = "start";
        public static final String COLUMN_END = "end";
        public static final String COLUMN_TIME_ELAPSED = "time_elapsed";
        public static final String COLUMN_ALGORITHM = "algorithm";
        public static final String COLUMN_PRIMES_COUNT = "primes_count";
        public static final String COLUMN_TIMESTAMP = "timestamp";
    }
}
