package prime;

import prime.generators.*;


public class PrimeNumberGeneratorFactory {
    private static final String ALGO_AUTO = "auto";
    private static final String ALGO_SIMPLE = "simple";
    private static final String ALGO_ESIEVE = "esieve";
    private static final String ALGO_SEG_ESIEVE = "segesieve";
    private static final String ALGO_PARALLEL_SEG_SIEVE = "parallel";

    /**
     * Factory method to get the appropriate algorithm according to the the range of numbers that need to be generated.
     *
     * @param end End of the range of primes required.
     * @return Object of the appropriate prime number generation algorithm.
     */
    public static PrimeNumberGenerator getGenerator(int end) {
        return getGenerator(PrimeNumberGeneratorFactory.ALGO_AUTO, end);
    }

    /**
     * Factory method to get the prime number generation algorithm specified in the param
     * @param algo The name of the algo required
     * @return Object of the prime number generation algorithm.
     */
    public static PrimeNumberGenerator getPrimeNumberGenerator(String algo) {
        switch (algo) {
            case ALGO_SIMPLE: return new SimplePrimeNumberGenerator();
            case ALGO_ESIEVE: return new SieveOfEratosthenes();
            case ALGO_SEG_ESIEVE: return new SegmentedSieveOfEratosthenes();
            case ALGO_PARALLEL_SEG_SIEVE: return new ParallelSegmentedSieveOfEratosthenes();
            default: return new SegmentedSieveOfEratosthenes();
        }
    }

    /**
     * Factory method to get the appropriate algorithm according to the the range of numbers that need to be generated
     * if the algo is specified to be auto or is null.
     *
     * @param end End of the range of primes required.
     * @return Object of the appropriate prime number generation algorithm.
     */
    public static PrimeNumberGenerator getGenerator(String algo, int end) {
        if (algo == null || algo.equalsIgnoreCase(ALGO_AUTO)) {
            if (end < 10e3) {
                algo = ALGO_ESIEVE;
            } else if (end < 10e5) {
                algo = ALGO_SEG_ESIEVE;
            } else {
                algo = ALGO_PARALLEL_SEG_SIEVE;
            }
        }

        return getPrimeNumberGenerator(algo);
    }
}
