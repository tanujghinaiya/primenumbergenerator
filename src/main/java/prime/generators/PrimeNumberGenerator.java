package prime.generators;

import prime.PrimeNumbers;

/**
 * Abstract class used for generating prime numbers.
 */
public abstract class PrimeNumberGenerator implements PrimeNumberGeneratorInterface {
    /**
     * Returns an instantiated object containing the lit of calculated prime numbers.
     *
     * This method also checks the input range of the parameter and makes sure that n1 and n2 are not inverse.
     *
     * @param n1 Starting point of the prime number search.
     * @param n2 End point of the prime number search.
     * @return PrimeNumber object with the resultant prime number
     */
    public PrimeNumbers generatePrimes(int n1, int n2) {
        long time = System.nanoTime();
        PrimeNumbers pns;

        // Checks if n1 is more than n2, if so, swaps them
        if(n1 > n2) {
            int temp = n1;
            n1 = n2;
            n2 = temp;
        }

        // If n2 less then two we return an empty list of primes, otherwise we call the method to calculate primes.
        if (n2 >= 2) {
            pns = calcPrimes(n1, n2);
        } else {
            pns = new PrimeNumbers(n1, n2);
        }

        // Calculation of the elapsed time
        time = System.nanoTime() - time;
        pns.setElapsedTime((float) (time / 10e6));

        return pns;
    }

    /**
     * Abstract method where the algorithms of prime calculation would reside.
     * @param n1 Starting point of the prime number search.
     * @param n2 End point of the prime number search.
     * @return PrimeNumber object with the resultant prime number
     */
    abstract PrimeNumbers calcPrimes(int n1, int n2);
}
