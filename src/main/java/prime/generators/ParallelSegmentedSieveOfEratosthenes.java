package prime.generators;

import prime.PrimeNumbers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

/**
 * A parallelized implementation of the Segmented Sieve of Eratosthenes
 */
public class ParallelSegmentedSieveOfEratosthenes extends PrimeNumberGenerator {

    /**
     * Implementation of a paralleilized version of Segmented Sieve of Eratosthenes which will run on n threads, if n
     * corea are available in the system. Refer: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
     *
     * @param n1 Starting point of the prime number search.
     * @param n2 End point of the prime number search.
     * @return PrimeNumber object with the resultant prime number
     */
    @Override
    public PrimeNumbers calcPrimes(int n1, int n2) {
        PrimeNumbers primeNumbers = new PrimeNumbers(n1, n2);
        primeNumbers.setGenerationAlgorithm(this.getClass());

        // Calculation sqrt of n2 followed by calculation of the primes will sqrt(n2) + 1
        int delta = (int) Math.sqrt(n2) + 1;
        final ArrayList<Integer> tempPrimes = new SieveOfEratosthenes().calcPrimes(0, delta).getPrimeNumbers();

        // Intantiating the thread pool to minimize context switching times.
        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        Set<Future<ArrayList<Integer>>> set = new HashSet<Future<ArrayList<Integer>>>();

        // Running prime generation for segments of size sqrt(n2) from sqrt(n2) + 1 in parallel.
        int i = 0;
        while (true) {
            final int start = n1 + i++ *  delta;
            final int end = start + delta - 1 > n2 ?  n2 : start + delta - 1;

            if (start >= end)
                break;

            // Using the segmented sieve method to eliminate non-primes
            Callable<ArrayList<Integer>> callable = () -> SegmentedSieveOfEratosthenes.segment(start, end, tempPrimes);
            Future<ArrayList<Integer>> future = pool.submit(callable);
            set.add(future);
        }

        // Aggregating the results of prime number search
        try {
            for (Future<ArrayList<Integer>> future : set) {
                primeNumbers.addAll(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        pool.shutdown();

        // Sorting the primes since the result may be out of order because of the parallelized operation
        Collections.sort(primeNumbers.getPrimeNumbers());

        return primeNumbers;
    }
}
