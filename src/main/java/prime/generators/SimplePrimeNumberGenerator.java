package prime.generators;

import prime.PrimeNumbers;

/**
 * A simple algorithm to generate primes.
 */
public class SimplePrimeNumberGenerator extends PrimeNumberGenerator {
    /**
     * An algorithm to generate primes based on the divisibility test with some performance optimizations.
     *
     * @param n1 Starting point of the prime number search.
     * @param n2 End point of the prime number search.
     * @return PrimeNumber object with the resultant prime number
     */
    @Override
    public PrimeNumbers calcPrimes(int n1, int n2) {
        PrimeNumbers primeNumbers = new PrimeNumbers(n1, n2);
        primeNumbers.setGenerationAlgorithm(this.getClass());

        // Conducts the primality test for each number from n1 to n2
        for (int n = n1; n <= n2; n++) {
            if (isPrime(n))  {
                primeNumbers.add(n);
            }
        }

        return primeNumbers;
    }

    private static boolean isPrime(int n) {
        // Elimination of all even numbers (except 2) and numbers less than 2
        if (n == 2) {
            return true;
        } else if (n < 2 || n % 2 == 0) {
            return false;
        }

        // Iteration till sqrt(n) and checking if any odd factors exist.
        int max = (int) Math.sqrt(n);

        for (int j = 3; j <= max; j += 2) {
            if ((n % j) == 0) {
                return false;
            }
        }

        return true;
    }
}
