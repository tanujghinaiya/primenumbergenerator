package prime.generators;

import prime.PrimeNumbers;

import java.util.Arrays;

/**
 * Implementation of the Sieve of Eratosthenes
 */
public class SieveOfEratosthenes extends PrimeNumberGenerator {
    /**
     * Implementation of the fast prime number generation algorithm, Sieve of Eratosthenes.
     * Refer: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
     *
     * @param n1 Starting point of the prime number search.
     * @param n2 End point of the prime number search.
     * @return PrimeNumber object with the resultant prime number
     */
    @Override
    public PrimeNumbers calcPrimes(int n1, int n2) {
        PrimeNumbers primeNumbers = new PrimeNumbers(n1, n2);
        primeNumbers.setGenerationAlgorithm(this.getClass());

        // Limiting max to sqrt(n2) since a prime cannot be larger than that
        int max = (int) Math.sqrt(n2);
        boolean[] primes = new boolean[(int) (n2 + 1)];

        Arrays.fill(primes, Boolean.TRUE);

        // Eliminating multiples of numbers starting from 2. The remaining numbers are primes
        for (int i = 2; i <= max; i++) {
            if (primes[i]) {
                int j = i * i;

                while (j <= n2) {
                    primes[j] = false;
                    j += i;
                }
            }
        }

        for (int i = 2; i < primes.length; i++) {
            if (primes[i] && i >= n1) {
                primeNumbers.add(i);
            }
        }

        return primeNumbers;
    }
}
