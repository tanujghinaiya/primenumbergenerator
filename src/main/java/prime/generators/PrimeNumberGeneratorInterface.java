package prime.generators;

import prime.PrimeNumbers;

import java.util.ArrayList;

public interface PrimeNumberGeneratorInterface {
    public PrimeNumbers generatePrimes(int n1, int n2);
}
