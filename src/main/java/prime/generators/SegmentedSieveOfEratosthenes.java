package prime.generators;

import prime.PrimeNumbers;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Segmented version of the Sieve of Eratosthenes which has much lower memory requirements.
 */
public class SegmentedSieveOfEratosthenes extends PrimeNumberGenerator {
    /**
     * Implementation of the Segmented Sieve of Eratosthenes algorithm which handles the given range in chunks to reduce
     * memory consumption. Refer: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
     *
     * @param n1 Starting point of the prime number search.
     * @param n2 End point of the prime number search.
     * @return PrimeNumber object with the resultant prime number
     */
    @Override
    public PrimeNumbers calcPrimes(int n1, int n2) {
        PrimeNumbers primeNumbers = new PrimeNumbers(n1 ,n2);
        primeNumbers.setGenerationAlgorithm(this.getClass());

        // Generates primes till sqrt(n2) 1 using the normal Sieve Of Eratosthenes
        int delta = (int) Math.sqrt(n2) + 1;
        ArrayList<Integer> tempPrimes = new SieveOfEratosthenes().generatePrimes(0, delta).getPrimeNumbers();

        // Segments are created of size sqrt(n2) and prime numbers are searced
        int i = 0;
        while (true) {
            int start = n1 + i++ *  delta;
            int end = start + delta - 1> n2 ?  n2 : start + delta - 1;

            if (start >= end)
                break;

            primeNumbers.addAll(segment(start, end, tempPrimes));
        }

        return primeNumbers;
    }

    /**
     * Method to handle a segment of the range of numbers to retrieve prime numbers
     *
     * @param n1 Starting point of the segment
     * @param n2 End point of the segment
     * @param tempPrimes Precalculated primes till the (sqrt + 1) of the end point of the required prime number range.
     * @return An ArrayList of primes in the segment.
     */
    public static ArrayList<Integer> segment(int n1, int n2, ArrayList<Integer> tempPrimes) {
        ArrayList<Integer> primeNumbers = new ArrayList<Integer>();
        boolean[] primes = new boolean[(int) (n2 - n1 + 1)];

        Arrays.fill(primes, Boolean.TRUE);

        // All multiple of the pre-calculated primes are marked as false
        for (int prime : tempPrimes) {
            int j = n1 / prime * prime;

            while (j <= n2) {
                if (j >= n1 && primes[j - n1] && prime != j) {
                    primes[j - n1] = false;
                }
                j += prime;
            }
        }

        // Elements which still remain in the primes array are primes.
        // loop to add all numbers to an ArrayList of primes where the number is given by i + n
        for (int i = 0; i < primes.length; i++) {
            if (primes[i] && (i + n1) > 1) {
                primeNumbers.add(i + n1);
            }
        }
        return primeNumbers;
    }
}
