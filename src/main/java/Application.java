import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.validators.PositiveInteger;
import db.DB;
import db.DBHelper;
import io.vertx.core.*;
import io.vertx.core.http.impl.HeadersAdaptor;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.logging.LogDelegate;
import io.vertx.ext.web.Router;
import prime.PrimeNumberGeneratorFactory;
import prime.PrimeNumbers;
import prime.generators.PrimeNumberGeneratorInterface;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Prime Number Generation application which can be used either as a REST service or a command line tool.
 * If -s is passed as an argument, the REST service will be started at http://<url>:8888/primes
 * if -n1 <start> -n2 <end> are passed it prints all the primes in the stdout.
 */
public class Application extends AbstractVerticle {
    @Parameter(names = {"-s", "--run-server"}, description = "Option to Run the rest server")
    private boolean runRestServer = false;

    @Parameter(names = {"-start", "-n1"}, description = "Start of the required range of prime numbers", validateWith = PositiveInteger.class)
    private int start = -1;

    @Parameter(names = {"-end", "-n2"}, description = "End of the required range of prime numbers")
    private int end = -1;

    @Parameter(names = {"-a", "-algo"}, description = "Choice of the prime number generation algorithms. (simple, esieve, segesieve, parallel)")
    private String algo = "auto";

    Logger logger = LoggerFactory.getLogger(Application.class.getSimpleName());

    @Override
    public void start(Future<Void> startFuture) {
        try {
            initializePrimeNumberGenerationRestService(startFuture);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initializePrimeNumberGenerationRestService(Future<Void> startFuture) throws SQLException, IOException, ClassNotFoundException {
        System.out.println("Starting prime number generator...");

        DBHelper helper = new DBHelper();
        helper.createTables();

        Router router = Router.router(vertx);
        router.route("/").handler(con -> {
            logger.info("Request to /, 301 redirect to /primes");
            con.response()
                    .setStatusCode(301)
                    .putHeader("Location", con.request().absoluteURI() + "primes")
                    .end();
        });
        router.route("/primes").handler(new PrimeNumbersHandler());

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(8888, resp -> {
                    if (resp.succeeded()) {
                        startFuture.succeeded();
                        System.out.println("Prime number generator started.");
                    } else {
                        startFuture.fail("Prime number generator failed to start.");
                    }
                });
    }

    public static void main(String[] args) {
        Application app = new Application();
        JCommander.newBuilder().addObject(app).build().parse(args);;

        if (app.runRestServer) {
            // Initialzing the in-memory database
            Thread t = new Thread(new DB());
            t.start();

            // Initializing the REST Server
            Vertx vertx = Vertx.vertx();
            vertx.deployVerticle(app);

            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (app.end > 0) {
            // Handling the CLI app
            PrimeNumberGeneratorInterface png = PrimeNumberGeneratorFactory.getGenerator(app.algo, app.end);
            PrimeNumbers pn = png.generatePrimes(app.start, app.end);
            System.out.println("Start : " + pn.getStart() + "  End : " + pn.getEnd());
            System.out.println(pn);
        } else {
            System.out.print("Usage:\n\tjava -jar target/prime-num-gen-jar-with-dependencies.jar -s (for REST Server)\n" +
                    "or\n" +
                    "\tjava -jar target/prime-num-gen-jar-with-dependencies.jar [options] (for CLI)\n");
            System.out.print("Where options include:\n" +
                    "-start\t: Beginning of the prime number range (reqqired)\n" +
                    "-end\t: End of the prime number range (reqqired)\n" +
                    "-algo\t: Manual selection of algorithm (simple esieve, segesieve, parallel) (optional)");
        }
    }
}
