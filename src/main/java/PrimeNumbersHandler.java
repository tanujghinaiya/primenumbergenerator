import db.DBHelper;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import prime.PrimeNumberGeneratorFactory;
import prime.PrimeNumbers;
import prime.generators.PrimeNumberGenerator;
import prime.generators.PrimeNumberGeneratorInterface;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Tanuj on 6/7/2017.
 */
public class PrimeNumbersHandler implements Handler<RoutingContext> {
    /**
     * Handler to handle requests on the REST Service
     *
     * Returns a json array of prime number request entries or Serialized PrimeNumbers response if the start and end are
     * specified. If algo is specified, that would be used for prime number generation.
     * @param routingContext
     */
    @Override
    public void handle(RoutingContext routingContext) {
        MultiMap map = routingContext.request().params();
        DBHelper dbHelper = new DBHelper();
        String response = "";

        if (map.get("end") != null) {
            String algo = map.get("algo");
            int start = map.get("start") == null ? 0 : Integer.parseInt(map.get("start"));
            int end = Integer.parseInt(map.get("end"));

            // Initializing the prime number generator and performing the generation
            PrimeNumberGenerator png = PrimeNumberGeneratorFactory.getGenerator(algo, end);
            PrimeNumbers pn = png.generatePrimes(start, end);

            response = Json.encodePrettily(pn);

            try {
                dbHelper.insertPrimeNumbersEntry(pn);
            } catch (SQLException | IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            try {
                ArrayList<PrimeNumbers> primeEntries = dbHelper.getAllPrimeNumberEntries();

                response = Json.encodePrettily(primeEntries);
            } catch (SQLException | IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        routingContext.response().putHeader("content-type", "application/json; charset=utf-8").end(response);
    }
}
