import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        PrimeNumberGeneratorTests.class
})

/**
 * Created by Tanuj on 6/7/2017.
 */
public class AllTests {
}
