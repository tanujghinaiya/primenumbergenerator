import org.junit.Test;
import prime.PrimeNumberGeneratorFactory;
import prime.PrimeNumbers;
import prime.generators.*;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tanuj on 6/7/2017.
 */
public class PrimeNumberGeneratorTests {
    private static final ArrayList<Integer> primeNumbersBelow1k = new ArrayList<Integer>(Arrays.asList(
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103,
            107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223,
            227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347,
            349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463,
            467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607,
            613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743,
            751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883,
            887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997));

    private void printResult(PrimeNumberGenerator png, PrimeNumbers primeNumbers) {
        System.out.printf("Runtime: %.4f \t Found %d primes in range %d to %d \t(%s)\n", primeNumbers.getElapsedTime(),
                primeNumbers.size(), primeNumbers.getStart(), primeNumbers.getEnd(), png.getClass().getSimpleName());
    }

    @Test
    public void testSimplePrimeNumberGenerator() {
        PrimeNumberGenerator png = new SimplePrimeNumberGenerator();
        PrimeNumbers primeNumbers = png.generatePrimes(1, 1000);

        printResult(png, primeNumbers);

        assertEquals(168, primeNumbers.size());
        assertTrue(primeNumbersBelow1k.equals(primeNumbers.getPrimeNumbers()));
    }

    @Test
    public void testSieveOfEratosthenes() {
        PrimeNumberGenerator png = new SieveOfEratosthenes();
        PrimeNumbers primeNumbers = png.generatePrimes(1, 1000);

        assertEquals(168, primeNumbers.size());
        assertTrue(primeNumbersBelow1k.equals(primeNumbers.getPrimeNumbers()));
    }

    @Test
    public void testSegmentedSieveOfEratosthenes() {
        PrimeNumberGenerator png = new SegmentedSieveOfEratosthenes();
        PrimeNumbers primeNumbers = png.generatePrimes(1, 1000);

        printResult(png, primeNumbers);

        assertEquals(168, primeNumbers.size());
        assertTrue(primeNumbersBelow1k.equals(primeNumbers.getPrimeNumbers()));
    }

    @Test
    public void testParallelSegmentedSieveOfEratosthenes() {
        PrimeNumberGenerator png = new ParallelSegmentedSieveOfEratosthenes();
        PrimeNumbers primeNumbers = png.generatePrimes(1, 1000);

        printResult(png, primeNumbers);

        assertEquals(168, primeNumbers.size());
        assertTrue(primeNumbersBelow1k.equals(primeNumbers.getPrimeNumbers()));
    }

    @Test
    public void testInverseN1N2() {
        int n1 = 1000;
        int n2 = 1;

        PrimeNumberGenerator png = PrimeNumberGeneratorFactory.getGenerator(n2);
        PrimeNumbers primeNumbers = png.generatePrimes(n1, n2);

        printResult(png, primeNumbers);

        assertEquals(168, primeNumbers.size());
        assertTrue(primeNumbersBelow1k.equals(primeNumbers.getPrimeNumbers()));
    }

    @Test
    public void testNegativeN1() {
        int n1 = -1;
        int n2 = 1000;

        PrimeNumberGenerator png = PrimeNumberGeneratorFactory.getGenerator(n2);
        PrimeNumbers primeNumbers = png.generatePrimes(n1, n2);

        printResult(png, primeNumbers);

        assertEquals(168, primeNumbers.size());
        assertTrue(primeNumbersBelow1k.equals(primeNumbers.getPrimeNumbers()));
    }

    @Test
    public void testNegativeN2() {
        int n1 = 0;
        int n2 = -100;

        PrimeNumberGenerator png = PrimeNumberGeneratorFactory.getGenerator(n2);
        PrimeNumbers primeNumbers = png.generatePrimes(n1, n2);

        printResult(png, primeNumbers);

        assertEquals(0, primeNumbers.size());
        assertTrue(primeNumbers.getPrimeNumbers().isEmpty());
    }

    @Test
    public void testNegativeN1N2() {
        int n1 = -200;
        int n2 = -100;

        PrimeNumberGenerator png = PrimeNumberGeneratorFactory.getGenerator(n2);
        PrimeNumbers primeNumbers = png.generatePrimes(n1, n2);

        printResult(png, primeNumbers);

        assertEquals(0, primeNumbers.size());
        assertTrue(primeNumbers.getPrimeNumbers().isEmpty());
    }

    @Test
    public void testPrimeNumberGeneratorFactory() {
        PrimeNumberGenerator png = PrimeNumberGeneratorFactory.getGenerator(10);
        assertEquals(png.getClass(), SieveOfEratosthenes.class);

        png = PrimeNumberGeneratorFactory.getGenerator((int) 10e4);
        assertEquals(png.getClass(), SegmentedSieveOfEratosthenes.class);

        png = PrimeNumberGeneratorFactory.getGenerator((int) 10e6);
        assertEquals(png.getClass(), ParallelSegmentedSieveOfEratosthenes.class);
    }
}
