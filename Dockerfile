FROM maven:3.5-jdk-8 as build
WORKDIR /maven-build/
ADD . /maven-build/
RUN mvn clean install

#FROM openjdk:8-jre
FROM nimmis/alpine-java:oracle-8-jre
WORKDIR /app
COPY --from=build /maven-build/target/prime-num-gen-jar-with-dependencies.jar .
CMD ["java", "-jar", "/app/prime-num-gen-jar-with-dependencies.jar", "-s"]
